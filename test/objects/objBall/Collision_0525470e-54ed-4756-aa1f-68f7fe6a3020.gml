/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 655E06B6
/// @DnDApplyTo : other
with(other) instance_destroy();

/// @DnDAction : YoYo Games.Instance Variables.Set_Score
/// @DnDVersion : 1
/// @DnDHash : 791BC0B7
/// @DnDApplyTo : d0604cbc-17fb-43a6-b831-3d58b80f7480
/// @DnDArgument : "score" "+1"
/// @DnDArgument : "score_relative" "1"
with(objRules) {
if(!variable_instance_exists(id, "__dnd_score")) __dnd_score = 0;
__dnd_score += real(+1);
}