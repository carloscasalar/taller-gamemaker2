{
    "id": "ade629d8-6b65-40ab-b188-28d2c8747d83",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objBall",
    "eventList": [
        {
            "id": "6e390c50-1713-48fd-bae8-342a8c8771ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 39,
            "eventtype": 5,
            "m_owner": "ade629d8-6b65-40ab-b188-28d2c8747d83"
        },
        {
            "id": "45d958eb-a0aa-4410-8ea9-a44e236487e7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 37,
            "eventtype": 5,
            "m_owner": "ade629d8-6b65-40ab-b188-28d2c8747d83"
        },
        {
            "id": "33916342-debc-4af7-96b5-b2abf6874954",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 5,
            "m_owner": "ade629d8-6b65-40ab-b188-28d2c8747d83"
        },
        {
            "id": "9f094a86-dcf8-40e7-bb89-b579216ec137",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 38,
            "eventtype": 5,
            "m_owner": "ade629d8-6b65-40ab-b188-28d2c8747d83"
        },
        {
            "id": "0525470e-54ed-4756-aa1f-68f7fe6a3020",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "28c7db55-c6c6-48a9-8f9b-c0cd9161c565",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ade629d8-6b65-40ab-b188-28d2c8747d83"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "a46032bd-8ff2-450a-9576-862f4cd238df",
    "visible": true
}