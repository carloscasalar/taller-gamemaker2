{
    "id": "dc9df544-8258-4935-84d5-33a114451f3b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objEnemy",
    "eventList": [
        {
            "id": "0c5ba74f-6ad8-4771-8d0f-4d7e9d71a16f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "ade629d8-6b65-40ab-b188-28d2c8747d83",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "dc9df544-8258-4935-84d5-33a114451f3b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "e4676292-b911-425a-ba84-2b9704f34209",
    "visible": true
}