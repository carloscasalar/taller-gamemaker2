/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 42AED249
/// @DnDApplyTo : other
with(other) instance_destroy();

/// @DnDAction : YoYo Games.Common.Set_Global
/// @DnDVersion : 1
/// @DnDHash : 01A0272E
/// @DnDArgument : "value" "1"
/// @DnDArgument : "var" "gameOverTag"
global.gameOverTag = 1;