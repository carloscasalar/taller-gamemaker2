/// @DnDAction : YoYo Games.Drawing.Draw_Instance_Score
/// @DnDVersion : 1
/// @DnDHash : 4EAF1039
if(!variable_instance_exists(id, "__dnd_score")) __dnd_score = 0;
draw_text(0, 0, string("Score: ") + string(__dnd_score));

/// @DnDAction : YoYo Games.Common.If_Expression
/// @DnDVersion : 1
/// @DnDHash : 200992BE
/// @DnDArgument : "expr" "global.gameOverTag == 1"
if(global.gameOverTag == 1)
{
	/// @DnDAction : YoYo Games.Drawing.Set_Color
	/// @DnDVersion : 1
	/// @DnDHash : 77A71F5B
	/// @DnDParent : 200992BE
	/// @DnDArgument : "color" "$FF0000FF"
	draw_set_colour($FF0000FF & $ffffff);
	draw_set_alpha(($FF0000FF >> 24) / $ff);

	/// @DnDAction : YoYo Games.Drawing.Set_Alignment
	/// @DnDVersion : 1.1
	/// @DnDHash : 068C2752
	/// @DnDParent : 200992BE
	/// @DnDArgument : "halign" "fa_center"
	/// @DnDArgument : "valign" "fa_middle"
	draw_set_halign(fa_center);
	draw_set_valign(fa_middle);

	/// @DnDAction : YoYo Games.Drawing.Draw_Value
	/// @DnDVersion : 1
	/// @DnDHash : 66DE8BAC
	/// @DnDParent : 200992BE
	/// @DnDArgument : "x" "320"
	/// @DnDArgument : "y" "180"
	/// @DnDArgument : "caption" ""GAME OVER""
	draw_text(320, 180, string("GAME OVER") + "");
}

/// @DnDAction : YoYo Games.Common.Else
/// @DnDVersion : 1
/// @DnDHash : 16DF6EEB
else
{

}

/// @DnDAction : YoYo Games.Instances.Get_Alarm
/// @DnDVersion : 1
/// @DnDHash : 1D5B9FCF
/// @DnDArgument : "var" "alarmCountdown"
alarmCountdown = alarm_get(0);

/// @DnDAction : YoYo Games.Drawing.Draw_Value
/// @DnDVersion : 1
/// @DnDHash : 724AD419
/// @DnDArgument : "y" "100"
/// @DnDArgument : "caption" ""TIme: ""
/// @DnDArgument : "var" "alarmCountdown"
draw_text(0, 100, string("TIme: ") + string(alarmCountdown));