{
    "id": "d0604cbc-17fb-43a6-b831-3d58b80f7480",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objRules",
    "eventList": [
        {
            "id": "3adb418b-b4cc-488a-8b59-2b9d2d82a256",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "d0604cbc-17fb-43a6-b831-3d58b80f7480"
        },
        {
            "id": "98bc66ff-c962-4206-b5a4-29881966943c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d0604cbc-17fb-43a6-b831-3d58b80f7480"
        },
        {
            "id": "4b49aa21-0ec2-45ac-ad3d-9abe1329b1ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "d0604cbc-17fb-43a6-b831-3d58b80f7480"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}