{
    "id": "e8484e4e-2595-4834-8d70-9b8407434433",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprBG",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 676,
    "bbox_left": 0,
    "bbox_right": 865,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "93a33205-01ee-4aa5-a37c-2aa2e739e472",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8484e4e-2595-4834-8d70-9b8407434433",
            "compositeImage": {
                "id": "c063733c-699a-4efe-90ce-f7fa144b4a70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93a33205-01ee-4aa5-a37c-2aa2e739e472",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f150795a-f12d-44a8-873e-6ee6cdb7243c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93a33205-01ee-4aa5-a37c-2aa2e739e472",
                    "LayerId": "5262ac42-3423-477c-839e-a21531af86ed"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 677,
    "layers": [
        {
            "id": "5262ac42-3423-477c-839e-a21531af86ed",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e8484e4e-2595-4834-8d70-9b8407434433",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 866,
    "xorig": 0,
    "yorig": 0
}