{
    "id": "a46032bd-8ff2-450a-9576-862f4cd238df",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprBall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 1,
    "bbox_right": 62,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "frames": [
        {
            "id": "d4518795-f542-420d-9c19-f89b6885ab86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a46032bd-8ff2-450a-9576-862f4cd238df",
            "compositeImage": {
                "id": "de8638b3-82bc-4c7e-a4ab-30d0aa9240ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4518795-f542-420d-9c19-f89b6885ab86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dafa9398-43b5-47cc-9a52-58749a7f514d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4518795-f542-420d-9c19-f89b6885ab86",
                    "LayerId": "8a5e6e8a-7eaf-4d85-ad79-f8db82fe62a4"
                },
                {
                    "id": "9266c880-0801-48d0-b725-08a0e1529712",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4518795-f542-420d-9c19-f89b6885ab86",
                    "LayerId": "f55d45da-8ee1-4910-8dfa-60dc30f31bd1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f55d45da-8ee1-4910-8dfa-60dc30f31bd1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a46032bd-8ff2-450a-9576-862f4cd238df",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "8a5e6e8a-7eaf-4d85-ad79-f8db82fe62a4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a46032bd-8ff2-450a-9576-862f4cd238df",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}