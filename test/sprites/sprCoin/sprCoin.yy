{
    "id": "3b023e08-ef07-493f-bcb1-e378bf1a576b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprCoin",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 50,
    "bbox_left": 17,
    "bbox_right": 48,
    "bbox_top": 16,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "frames": [
        {
            "id": "8391e637-ae59-4f72-85f1-a055ad65c934",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b023e08-ef07-493f-bcb1-e378bf1a576b",
            "compositeImage": {
                "id": "b7e9db37-8c04-4500-bf26-f15a0a0a82f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8391e637-ae59-4f72-85f1-a055ad65c934",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0a6211e-3128-42a4-8847-5beb1eb1227d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8391e637-ae59-4f72-85f1-a055ad65c934",
                    "LayerId": "67c65665-d86b-4af6-bced-710556e274cc"
                }
            ]
        },
        {
            "id": "832c5d38-9c84-4c0b-8c0f-e0999d70f781",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b023e08-ef07-493f-bcb1-e378bf1a576b",
            "compositeImage": {
                "id": "361d30fa-d108-4e82-a29b-ce66f0598ad1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "832c5d38-9c84-4c0b-8c0f-e0999d70f781",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27318d25-2483-4b0f-9419-3f6f11ed89b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "832c5d38-9c84-4c0b-8c0f-e0999d70f781",
                    "LayerId": "67c65665-d86b-4af6-bced-710556e274cc"
                }
            ]
        },
        {
            "id": "1a834c81-90e6-40e4-aac4-f54463ab8b7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b023e08-ef07-493f-bcb1-e378bf1a576b",
            "compositeImage": {
                "id": "3695b8b7-9d1e-4fa7-9027-cecbd07224f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a834c81-90e6-40e4-aac4-f54463ab8b7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9c52e42-86cd-446d-93db-758ab65fe085",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a834c81-90e6-40e4-aac4-f54463ab8b7c",
                    "LayerId": "67c65665-d86b-4af6-bced-710556e274cc"
                }
            ]
        },
        {
            "id": "ae920a17-6d43-4190-84f6-1b8c5563efa9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b023e08-ef07-493f-bcb1-e378bf1a576b",
            "compositeImage": {
                "id": "dbaa895b-9e03-4838-a7eb-4b2108ff341f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae920a17-6d43-4190-84f6-1b8c5563efa9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "181b4cde-2175-43ae-840f-6c87ed01d1da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae920a17-6d43-4190-84f6-1b8c5563efa9",
                    "LayerId": "67c65665-d86b-4af6-bced-710556e274cc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "67c65665-d86b-4af6-bced-710556e274cc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3b023e08-ef07-493f-bcb1-e378bf1a576b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}