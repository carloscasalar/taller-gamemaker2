{
    "id": "e4676292-b911-425a-ba84-2b9704f34209",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprEnemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 67,
    "bbox_left": 1,
    "bbox_right": 58,
    "bbox_top": -1,
    "bboxmode": 2,
    "colkind": 3,
    "coltolerance": 0,
    "frames": [
        {
            "id": "74e3079d-9a24-4a01-af40-f3f67af79d0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4676292-b911-425a-ba84-2b9704f34209",
            "compositeImage": {
                "id": "65998819-49b5-4816-b05d-920e41cee0d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74e3079d-9a24-4a01-af40-f3f67af79d0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db07e3cb-3ee3-43d2-83a7-068a9e876ae5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74e3079d-9a24-4a01-af40-f3f67af79d0c",
                    "LayerId": "be8a0b0b-e819-459b-9c0b-cfca4352cf8a"
                }
            ]
        },
        {
            "id": "43841c9d-cf53-4a2c-8b66-06db03db1afb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4676292-b911-425a-ba84-2b9704f34209",
            "compositeImage": {
                "id": "923edd38-300a-44c2-b799-a7b8bcae7ab6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43841c9d-cf53-4a2c-8b66-06db03db1afb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b1831e7-7235-4233-b1b1-1ba76958f159",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43841c9d-cf53-4a2c-8b66-06db03db1afb",
                    "LayerId": "be8a0b0b-e819-459b-9c0b-cfca4352cf8a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "be8a0b0b-e819-459b-9c0b-cfca4352cf8a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e4676292-b911-425a-ba84-2b9704f34209",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 33,
    "yorig": 26
}